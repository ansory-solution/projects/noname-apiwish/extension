// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
// This File is From Ansory Solution
// Licensed under BSD-2-Clause
// File: std.js (ansory-solution/projects/noname-apiwish/extension/src/std.js)
// Content:	
// Copyright (c) 2023 Ansory Solution All rights reserved
// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

ansory.std = new class AnsoryStandard {
	/**
	 * @param {function(GameEvent, number, Player, Player, Player, [Player], Card, [Card], string, boolean, number, GameEvent, BaseCommonResultData): void} func
	 * @returns {function(void): void}
	 */
	revParsex(func) {
		const str = func.toString();
		const regStart = ndzo.escape("function anonymous(event,step,source,player,target,targets,card,cards,skill,forced,num,trigger,result,_status,lib,game,ui,get,ai\n) {\n");
		const regEnd = ndzo.escape("}");
		// 如果没有步骤
		if (/\{if\(event.step==1\) \{event.finish\(\);return;}.+}/s.test(str)) {
			const reg = new RegExp(`${regStart}${ndzo.escape("{if(event.step==1) {event.finish();return;}")}\n?(\\s*)(.*)\n?${regEnd}\n?${regEnd}`, "s");
			const matchResult = str.match(reg);
			return new Function(`${matchResult[1]}${matchResult[2].trim()}`);
		}
		// 否则就开始补充步骤
		else {
			let matchResult;
			const first = `${regStart}\n?(.*)\n?(\\s*)${ndzo.escape("if(event.step==")}(\\d{1,2})${ndzo.escape(") {event.finish();return;}switch(step){")}`
			const firstReg = new RegExp(`${first}case 0:`, "s");
			matchResult = str.match(firstReg);
			const stepNumber = parseInt(matchResult[matchResult.length - 1]);

			const middle = Array.from(Array(stepNumber).keys()).map(item => `case ${item}:`).join(`(.*)\\s*break;`);
			const final = `(.+)\n?}\n?${regEnd}`;

			const reg = new RegExp(`${first}${middle}${final}`, "s");

			matchResult = str.match(reg);
			const result = matchResult.slice(4);

			return new Function(`${matchResult[1]}${result.map((item, index) => `${matchResult[2]}"step ${index}"${item}`).join("").trim()}`);
		}
	}

	/**
	 * @async
	 * @param {[function(...any): any, function(any): boolean, ...any] | [true]} lists
	 * @param {(function(...any): any) | (function(...any): Promise<any>)} [success]
	 * @param {(function(...any): any) | (function(...any): Promise<any>)} [failure]
	 * @param {boolean} [isAwait = false]
	 * @returns {Promise<boolean>}
	 */
	async with(lists, success, failure, isAwait = false) {
		let bool = true, result = null
		for (let item of lists) {
			if (ndzp.e(true, item)) break;
			result = await item[0](result, ...item.slice(2));
			if (typeof item[1] == "function") {
				if (!(await item[1](result))) {
					bool = false;
					break;
				}
			}
		}
		if (bool) {
			if (success) {
				if (isAwait) await success(result);
				else success(result);
			}
			return true;
		}
		else {
			if (failure) {
				if (isAwait) await failure();
				else failure();
			}
			return false;
		}
	}
}
