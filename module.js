// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
// This File is From Ansory Solution
// Licensed under BSD-2-Clause
// File: module.js (ansory-solution/projects/noname-apiwish/extension/module.js)
// Content:	
// Copyright (c) 2023 Ansory Solution All rights reserved
// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

const {lib} = window.noname;

await Promise.all([
	new Promise(async resolve => {
		await ndz.require(lib.assetURL + "extension/wishAPI/src", "ansory", "module");

		Promise.all([
			ndz.require(lib.assetURL + "extension/wishAPI/src/ansory", "std", "module"),
			ndz.require(lib.assetURL + "extension/wishAPI/src/ansory", "env", "module")
		]).then(resolve);
	}),
	ndz.require(lib.assetURL + "extension/wishAPI/src", "polyfill", "module")
]);

Reflect.set(lib.ansorial, "status", true);
