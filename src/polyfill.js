// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
// This File is From Ansory Solution
// Licensed under BSD-2-Clause
// File: polyfill.js (ansory-solution/projects/noname-apiwish/extension/src/polyfill.js)
// Content:
// Copyright (c) 2023 Ansory Solution All rights reserved
// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

{
	NodeList.prototype.contains = function (item) {
		for (let i = 0; i < this.length; ++i)
			if (ndzp.e(item, this[i]))
				return true;

		return false;
	}
}
