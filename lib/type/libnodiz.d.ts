// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
// This File is From Ansory Solution
// Licensed under BSD-2-Clause
// File: libnodiz.d.ts (ansory-solution/projects/libnodiz/libnodiz.d.ts)
// Content:	Ansory Dizaster Lib Type Declare
// Copyright (c) 2023 Ansory Solution All rights reserved
// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

declare var libnodiz: Libnodiz;
declare var ndz: Libnodiz;
declare var ndzc: Libnodiz.Creation;
declare var ndzd: Libnodiz.DOM;
declare var ndzdg: Libnodiz.DOM.Generate;
declare var ndzdi: Libnodiz.DOM.Init;
declare var ndzf: Libnodiz.Fonts;
declare var ndzfs: Libnodiz.Files;
declare var ndzo: Libnodiz.Others;
declare var ndzp: Libnodiz.Comparator;
declare var ndzs: Libnodiz.Style;

interface Libnodiz {
	comparator: LibnodizComparator;
	creation: LibnodizCreation;
	dom: LibnodizDOM;
	fonts: LibnodizFonts;
	files: LibnodizFiles;
	others: LibnodizOthers;
	style: LibnodizStyle;

	/**
	 * setTimeout参数改版，但无返回值
	 *
	 * 其实没啥用，因为LibnodizTimer用不了，但ndz.delay看着好看
	 *
	 * @param ms - 等待的毫秒数
	 * @param args - 需要传入setTimeout函数的参数
	 */
	delay(ms: number, ...args: any[]): void;

	/**
	 * 将object B的所有可遍历项的值全赋值到object A中，或者可以理解为面向对象中的继承。
	 *
	 * @param child - object A
	 * @param father - object B
	 */
	doSuper(child: object, father: object): void;

	/**
	 * 创建并加载一个Script
	 *
	 * 需处理脚本位置
	 *
	 * @param path - 目录
	 * @param file - 文件
	 * @param [type = "common"] - 加载形式（CommonJS和ES6 Module）
	 */
	require(path: string, file: string, type?: "common" | "module"): Promise<HTMLScriptElement>;

	/**
	 * setTimeout的Promise封装
	 *
	 * @param ms - 等待的毫秒数
	 * @param args - 需要传入setTimeout函数的参数
	 */
	sleep(ms: number, ...args: any[]): Promise<void>;

	/**
	 * 等待一个函数执行出正确结果（setInterval的随后clear封装）
	 *
	 * 返回值为函数的执行结果
	 *
	 * @async
	 * func - 需要执行的函数
	 */
	wait(func: ((...args: any[]) => any)): Promise<any>;

	/**
	 * 等待一个函数执行出正确结果，但只重试给定的次数
	 *
	 * 返回值为函数的执行结果
	 *
	 * @async
	 * @param time - 重试的次数
	 * @param func - 需要执行的函数
	 */
	retry(time: number, func: ((...args: any[]) => any)): Promise<any>;
}

// Ansory Dizaster Lib·希望灾难库
//
// 关于无名杀的一些原生杂项的代码前置库。
declare namespace Libnodiz {
	// Ansory Dizaster Lib - Comparator·希望灾难比较库（ndzp）
	//
	// 关于一些玄学的比较函数 (指不想打===但==会警告且不想管报错提醒）
	interface Comparator {
		/**
		 * 判断所给参数是否（与主体）一致
		 * @param sub - 主体
		 * @param objs - 需要判断的参数
		 */
		e(sub: any, ...objs: any[]): boolean

		/**
		 * 判断所给参数是否与主体不一致
		 * @param sub - 主体
		 * @param objs - 需要判断的参数
		 */
		ne(sub: any, ...objs: any[]): boolean
	}

	// Ansory Dizaster Lib - Creation·希望灾难创造库（ndzc）
	//
	// 关于一些玄学的创造函数（指不喜欢[]但new Array会警告又不想管报错提醒）
	interface Creation {
		/**
		 * 创建一个新数组
		 */
		get a(): Array<any>;

		/**
		 * 创建一个新Object
		 */
		get o(): Object;

		/**
		 * 创建一个没有构造器的Object
		 */
		get no(): Object;

		/**
		 * 创建一个字符串
		 */
		get s(): string;
	}

	interface DOM {
		generate: LibnodizDOMGenerate;
		init: LibnodizDOMInit;

		/**
		 * 将由ndzd.parse得到的object注入到一个dom中
		 *
		 * @param object - 需要注入到的DOM对象
		 * @param options - 需要注入的规则
		 */
		inject(object: HTMLElement, options: Object): void;

		/**
		 * 将给定的参数通过一定映射转化为一个无构造器的object
		 *
		 * @param args - 给定的参数
		 */
		parse(args: any[]): object;
	}

	// Ansory Dizaster Lib - Files·希望灾难文件库（ndzfs）
	//
	// 用于四端的简易文件管理（网页端待支持，主要是等待诗笺的授权（或者说什么时候我能打得过诗笺））（服务器端等诗笺的公布，或者就这么鸽了）
	interface Files {
		/**
		 * 读取一个文件的内容，效果同game.readFile
		 * Web端读取待定，Server端读取鸽置
		 * @param file - 文件名（及其目录的地址）（无需lib.asserURL）
		 * @returns {Promise<Buffer | ArrayBuffer>}
		 */
		readFile(file: string): Promise<Buffer | ArrayBuffer>
	}

	// Ansory Dizaster Lib - Files·希望灾难字体库（ndzf）
	//
	// 用于简易的字体加载管理（指不想因为这玩意写css但自己创又太麻烦）
	interface Fonts {
		/**
		 * 添加一个FontFace
		 * 此操作会同时将此字体加入document.fonts
		 * @async
		 * @param font - 需要添加的字体
		 */
		add(font: FontFace): Promise<boolean>;

		/**
		 * 以Object的形式将字体添加进document.fonts，并返回各自结果
		 * Object的key为字体名，value为src
		 * @param object - 需要加载的字体列表对象
		 */
		addFontsInObject(object: object): Promise<boolean>[];

		/**
		 * 检测是否添加过给定的字体
		 * @param font - 需要检测的字体
		 */
		has(font: FontFace): boolean;

		/**
		 * 检测是否添加过给定的字体
		 * @param font - 需要检测的字体
		 */
		isHas(font: FontFace): boolean;

		/**
		 * 移除一个已经添加的字体
		 * @param font - 需要移除的字体
		 */
		remove(font: FontFace): boolean;

		/**
		 * 等待字体全部加载完成（同document.fonts.ready)
		 */
		get waitReady(): Promise<boolean>;
	}

	// Ansory Dizaster Lib - Others·希望灾难杂项库（ndzo）
	//
	// 一些玄学的玩意，有些为了规避警告，有些为了可能的后续支持
	interface Others {
		/**
		 * filter中检测值是否为真值的函数
		 */
		get checkTrue(): (any) => boolean;

		/**
		 * 将string中的正则特殊符号转义
		 *
		 * @param {string} string
		 * @returns {string}
		 */
		escape(string: string): string;

		/**
		 * 一个没任何用的函数，但可以给Promise的then函数，让IDE不警告
		 *
		 * @param _args - 啥都行，反正不处理
		 */
		ignore(..._args: any[]): void;

		/**
		 * 效果同console.log，在没有其他想法前就是个没用的函数
		 *
		 * @param items - 需要打印的东西
		 */
		log(...items: any[]): void;

		/**
		 * 将一个值添加进一个数组中，并返回值本身
		 *
		 * @param ary - 要添加进的数组
		 * @param item - 要添加进数组的值
		 */
		toIn<T>(ary: Array<T>, item: T): T
	}

	// Ansory Dizaster Lib - Style·希望灾难样式库（ndzs）
	//
	// 简易且便于维护的动态CSS库，规避原生CSSStyleSheet不方便的insertRule和deleteRule
	interface Style {
		/**
		 * 将一条规则加进当前的样式里
		 * 如果类名存在，则更新其所对应的规则
		 * 此举动不会影响此类名原有的无光规则
		 * @param name - 相关类名
		 * @param style - 需要添加的规则
		 */
		add(name: string, style: object): boolean;

		/**
		 * 将一系列的规则添加到样式（key为类名，value为对应的样式）
		 * 添加方法为ndzs.add
		 * @param object - 需要添加的规则列表对象
		 */
		addObject(object: object): boolean[];

		/**
		 * 返回现有类名的数量
		 */
		get count(): number;

		/**
		 * 通过给定的信息寻找规则
		 * - 如果给的是数值，则寻找对应项的规则
		 * - 如果给的是函数，则寻找符合条件且项最小的规则
		 * @param something - 给定的信息
		 */
		find(something: number | (([name, style]: [string, object]) => boolean)): [string, object] | null;

		/**
		 * 将一条规则转化为css string
		 * @param name - 对应类名
		 * @param style - 类名对应的规则
		 */
		generate(name: string, style: object): string;

		/**
		 * 获取类名所对应的规则
		 * @param name - 给定的类名
		 */
		get(name: string): object | null;

		/**
		 * 由类名获取规则对应的项
		 * @param name - 给定的类名
		 */
		getIndex(name: string): number | null;

		/**
		 * 判定所给定的类名是否存在
		 * @param name - 需要判断的类名
		 */
		has(name: string): boolean;

		/**
		 * 判断一条所给的信息是否与一条规则对应
		 * - 如果给的是数值，则判断对应项是否存在（即规则数量大于所给定的值）
		 * - 如果给的是字符串，则判定对应的类名是否存在
		 * - 如果给的是函数，则判定是否存在符合条件的规则
		 * @param something - 提供的信息
		 */
		isHas(something: number | string | (([name, style]: [string, object]) => boolean)): boolean;

		/**
		 * 移除一条给定类名的规则
		 * @param name - 需要移除规则的类名
		 */
		remove(name: string): boolean;

		/**
		 * 将style object转化为string
		 *
		 * @param style - 需要转化的规则
		 */
		translate(style: object): string;

		/**
		 * 将一条规则转化为css string并加进所对应的style里
		 * 如果类名原先存在则会替换原有的规则
		 * @param name - 对应类名
		 * @param style - 类名对应的规则
		 */
		update(name: string, style: object): boolean;
	}
}

// Ansory Dizaster Lib - DOM·希望灾难元素库（ndzd）
//
// HTML元素的操控与创建相关，由动态生成创建元素的函数（无终止符的特殊字符除外）
declare namespace Libnodiz.DOM {
	// Ansory Dizaster Lib - DOM.Generate·希望灾难元素生成库（ndzdg）
	//
	// 用于生成HTML元素所对应的字符串，方便兼容以前不懂元素操控时写的代码（虽然没那种东西）
	interface Generate {
		/**
		 * 返回HTML中换行符的字符串
		 */
		get br(): string;

		/**
		 * 生成一个div字符串，参数参考Noname的形式
		 *
		 * 其参数列表:
		 * - 若为“object”类型（即json格式对象），则设置为style，即设置div的style
		 * - 若为“string”类型，则参考无名杀的处理方式给定属性或定义innerHTML
		 *
		 * @param args - 给定的参数
		 */
		div(...args: any[]): string;

		/**
		 * 生成一个span字符串，参数参考Noname的形式
		 *
		 * 其参数列表:
		 * - 若为“object”类型（即json格式对象），则设置为style，即设置div的style
		 * - 若为“string”类型，则参考无名杀的处理方式给定属性或定义innerHTML
		 *
		 * @param args - 给定的参数
		 */
		span(...args: any[]): string;

		/**
		 * 生成一个style字符串，参数参考Noname的形式
		 *
		 * 其参数列表:
		 * - 若为“object”类型（即json格式对象），则设置为style，即设置div的style
		 * - 若为“string”类型，则参考无名杀的处理方式给定属性或定义innerHTML
		 *
		 * @param args - 给定的参数
		 */
		style(...args: any[]): string;
	}

	// Ansory Dizaster Lib - DOM.Init·希望灾难元素初始库（ndzdi）
	//
	// 用于生成HTML元素
	interface Init {
		/**
		 * 返回一个HTML中的换行符
		 */
		get br(): HTMLBRElement;

		/**
		 * 生成一个div元素，参数参考Noname的形式
		 *
		 * 其参数列表:
		 * - 若为div，table，tr，td，body等这些类型html节点对象，则设置为position，即创建div即将插入到的父节点
		 * - 若为“object”类型（即json格式对象），则设置为style，即设置div的style
		 * - 若为“string”类型，则参考无名杀的处理方式给定属性或定义textContent
		 * - 若为“Array”类型，则将数组里面的内容添加进此元素的子节点中
		 * - 若为“Boolean”类型，则将“定义textContent”改为“定义innerHTML”
		 *
		 * @param args - 给定的参数
		 */
		div(...args: any[]): HTMLDivElement;

		/**
		 * 生成一个span元素，参数参考Noname的形式
		 *
		 * 其参数列表:
		 * - 若为div，table，tr，td，body等这些类型html节点对象，则设置为position，即创建div即将插入到的父节点
		 * - 若为“object”类型（即json格式对象），则设置为style，即设置div的style
		 * - 若为“string”类型，则参考无名杀的处理方式给定属性或定义textContent
		 * - 若为“Array”类型，则将数组里面的内容添加进此元素的子节点中
		 * - 若为“Boolean”类型，则将“定义textContent”改为“定义innerHTML”
		 *
		 * @param args - 给定的参数
		 */
		span(...args: any[]): HTMLSpanElement;

		/**
		 * 生成一个style元素，参数参考Noname的形式
		 *
		 * 其参数列表:
		 * - 若为div，table，tr，td，body等这些类型html节点对象，则设置为position，即创建div即将插入到的父节点
		 * - 若为“object”类型（即json格式对象），则设置为style，即设置div的style
		 * - 若为“string”类型，则参考无名杀的处理方式给定属性或定义textContent
		 * - 若为“Array”类型，则将数组里面的内容添加进此元素的子节点中
		 * - 若为“Boolean”类型，则将“定义textContent”改为“定义innerHTML”
		 *
		 * @param args - 给定的参数
		 */
		style(...args: any[]): HTMLStyleElement;
	}
}

type LibnodizComparator = Libnodiz.Comparator;
type LibnodizCreation = Libnodiz.Creation;
type LibnodizDOM = Libnodiz.DOM;
type LibnodizDOMGenerate = Libnodiz.DOM.Generate;
type LibnodizDOMInit = Libnodiz.DOM.Init;
type LibnodizFonts = Libnodiz.Fonts;
type LibnodizFiles = Libnodiz.Files;
type LibnodizOthers = Libnodiz.Others;
type LibnodizStyle = Libnodiz.Style;
