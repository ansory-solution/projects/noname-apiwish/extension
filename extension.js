/// <reference path="lib/type/index.d.ts" />
// noinspection JSUnresolvedVariable

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
// This File is From rExtension System
// Licensed under BSD-2-Clause
// File: extension.js (rExtension/libstd/extension.js)
// Content:	StandardLib Extension File
// Copyright (c) 2023 rExtension System All rights reserved
// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

"use strict"

// noinspection JSUnusedGlobalSymbols
game.import("extension", (lib, game, ui, get, ai, _status) => {
	window.noname = {
		lib, game, ui, get, ai, _status
	}

	if (!lib.ansorial) lib.ansorial = {};
	Reflect.defineProperty(lib.ansorial, "waitReady", {
		get() {
			if (Reflect.has(window, "ansory")) return true;
			return new Promise(async (resolve, reject) => {
				while (true) {
					if (Reflect.has(lib.ansorial, "status")) break;
					await new Promise(resolve2 => setTimeout(resolve2, 50));
				}
				if (lib.ansorial.status) resolve(true);
				else reject(false);
			});
		}
	});

	if (!lib.ansorial.ndz) lib.ansorial.ndz = {};
	Reflect.defineProperty(lib.ansorial.ndz, "waitReady", {
		get() {
			if (Reflect.has(window, "ndz")) return true;
			return new Promise(async (resolve, reject) => {
				while (true) {
					if (Reflect.has(lib.ansorial.ndz, "status")) break;
					await new Promise(resolve2 => setTimeout(resolve2, 50));
				}
				if (lib.ansorial.ndz.status) resolve(true);
				else reject(false);
			});
		}
	});

	const loading = new Promise((resolve, reject) => {
		lib.init.js(lib.assetURL + "extension/wishAPI/lib", "libnodiz.min", resolve, reject);
	});
	loading.then(_script => {
		Reflect.set(lib.ansorial.ndz, "status", true);
	}).catch(e => {
		Reflect.set(lib.ansorial.ndz, "status", false);
		throw e;
	});

	// noinspection JSUnusedGlobalSymbols
	return {
		name: "wishAPI",
		author: "Ansory Solution",
		intro: "Ansory 无名杀通用API库",
		package: {
			version: "0.1.0",
			branch: "Development",
			date: [2023, 2, 2, 1],
			build: 0,
			plan: 1
		},
		editable: false,
		config: {
			code: {
				name: "wishAPI的最后挣扎失效了/-\\",
				clear: true,
				nopointer: true
			},
			work: {
				name: "开启",
				init: true,
				intro: "开启后将加载wishAPI的功能代码",
			},
			debug: {
				name: "调试信息",
				init: false,
				intro: "开启后将于控制台输出扩展信息",
			}
			,
			installed_info: {
				name: "<div style='color: #6495ED; font-size: 12px; line-height: 14px; text-shadow: 0 0 2px black'>未安装wishAPI</div>",
				clear: true,
				nopointer: true
			}
			,
			info_bar: {
				name: "</br>",
				clear: true,
				nopointer: true
			},
			license: {
				name: null,
				info: null,
				clear: true
			},
			outfrom: {
				name: null,
				info: {
					"无名杀": {
						web: "https://github.com/libccy/noname",
						type: "c",
						content: null
					},
					"Ansory Dizaster Lib": {
						web: "https://gitlab.com/ansory-solution/projects/libnodiz",
						type: "e",
						content: null
					}
				},
				clear: true
			},
			copyrights_bar: {
				name: "</br>",
				clear: true,
				nopointer: true
			},
			copyrights: {
				name: ["<span style='color: #1688F2'><p style='text-align: center'>Copyright (c) 2023", "Ansory Solution", "All Rights Reserved</p></span>"].join("</br>"),
				// name: ["<div style='color: #1688F2; text-align: center'>Copyright (c) 2023", "Ansory Solution", "All Rights Reserved</div>"].join("</br>"),
				clear: true,
				nopointer: true
			},
			uninstall_bar: {
				name: "</br>",
				clear: true,
				nopointer: true
			},
			uninstall: {
				name: "删除此扩展",
				clear: true
				// NIY
			}
		},
		async content(config, pack) {
			await lib.ansorial.ndz.waitReady;

			const isEnable = config.work;

			if (config.debug) ndzo.log("wishAPI extension begins loading.");
			await ndz.require(lib.assetURL + "extension/wishAPI", "module", "module");
			if (config.debug) ndzo.log("wishAPI: Load file succeeded.");

			const [menuDiv, menuBarDiv] = await ndz.wait(() => {
				if (!ui.menuContainer || !ui.menuContainer.firstElementChild) return;
				const menu = ui.menuContainer.firstElementChild;
				const menuBar = menu.querySelector(".menu-tab");
				if (!menuBar) return;
				ansory.env.set("menuDiv", menu);
				ansory.env.set("menuBarDiv", menuBar);
				return [menu, menuBar];
			});

			const extDivList = menuBarDiv.childNodes[4]._link.childNodes[0];
			const readExtResult = await ndz.retry(20, () => {
				const [wishAPIDivLeft] = Array.from(extDivList.childNodes).filter(div => div.innerText === "wishAPI");
				if (!wishAPIDivLeft) return;
				const wishAPIDivRight = wishAPIDivLeft.link;
				if (!wishAPIDivRight) return;
				return [wishAPIDivLeft, wishAPIDivRight];
			});

			if (readExtResult) {
				const [wishAPIDivLeft, wishAPIDivRight] = readExtResult;

				wishAPIDivLeft.style.backgroundImage = `url("${lib.assetURL}extension/wishAPI/res/image/util/${isEnable ? "title_bar" : "title_bar_off"}.png")`;
				wishAPIDivLeft.style.backgroundSize = "100% 100%";
				wishAPIDivRight.style.backgroundColor = "rgba(28, 28, 28, 0.9)";
				wishAPIDivLeft.classList.remove("large");
				wishAPIDivLeft.style.fontFamily = `"ansory-name"`;
				wishAPIDivLeft.style.fontSize = "23px";
				wishAPIDivLeft.style.color = isEnable ? "#1C1C1C" : "#FFFFFF";
				let codeDiv = wishAPIDivRight.childNodes[0];
				codeDiv.classList.remove("on");
				codeDiv.removeChild(codeDiv.childNodes[0]);
				codeDiv.style.color = "#FFFFFF";

				const style = {
					height: "auto",
					// "text-align": "center",
					color: "#FFFFFF"
				};

				// 扩展信息
				let infoStyle = {
					...style,
					"text-align": "center",
					"font-family": "ansory-title"
				}
				if (!isEnable) infoStyle.color = "#808080";

				let animationStore = ndzc.no;
				animationStore.links = ndzc.a;

				ndzdi.span([
					ndzdi.div(".config", infoStyle, [
						ndzdi.br
					]),
					ndzo.toIn(animationStore.links, ndzdi.div(".config", {
						...infoStyle,
						"font-family": "ansory-symbol"
					}, `/-/ ${Array.from((new Array(13)).keys()).map(_ => "=").join(" ")} /-/`)),
					ndzdi.div(".config", infoStyle, [
						ndzdi.br
					]),
					ndzdi.div(".config", {...infoStyle, color: isEnable ? infoStyle.color : "#1688F2"}, [
						"Ansory Noname",
						ndzdi.br,
						"General API"
					]),
					ndzdi.div(".config", {
						...infoStyle,
						color: isEnable ? "#FF3333" : "#1688F2"
					}, [
						"Development Branch",
						ndzdi.br,
						`${pack.date[0]}.${`0${pack.date[1]}`.slice(-2)}.${`0${pack.date[2]}`.slice(-2)}.${`00${pack.date[3].toString(16).toUpperCase()}`.slice(-3)}`
					]),
					ndzdi.div(".config", infoStyle, [
						ndzdi.br
					]),
					ndzo.toIn(animationStore.links, ndzdi.div(".config", {
						...infoStyle,
						"font-family": "ansory-symbol"
					}, `/-/ ${Array.from((new Array(13)).keys()).map(_ => "=").join(" ")} /-/`)),
					ndzdi.div(".config", infoStyle, [
						ndzdi.br
					])
				], codeDiv);
				codeDiv.animation = animationStore;

				if (isEnable) {
					animationStore.index = 0;
					animationStore.strs = ["/-/", "---", "\\-\\", "|-|"];
					animationStore.times = [200, 50, 100, 50];
					animationStore.func = (self) => {
						for (const link of self.links) {
							link.textContent = `${self.strs[self.index]} ${Array.from((new Array(13)).keys()).map(_ => "=").join(" ")} ${self.strs[self.index]}`;
						}
						clearTimeout(self.interval);
						self.interval = setTimeout(self.func, self.times[self.index], self);
						if (++self.index > 3) self.index = 0;
					}

					const listener = new MutationObserver((..._args) => {
						if (wishAPIDivLeft.classList.contains("active")) {
							wishAPIDivLeft.style.backgroundImage = `url("${lib.assetURL}extension/wishAPI/res/image/util/title_bar_active.png")`;
							wishAPIDivLeft.style.color = "#E8E8E8";
							animationStore.interval = setTimeout(animationStore.func, 1, animationStore);
						} else {
							wishAPIDivLeft.style.backgroundImage = `url("${lib.assetURL}extension/wishAPI/res/image/util/title_bar.png")`;
							wishAPIDivLeft.style.color = "#1C1C1C";
							if (animationStore.interval) {
								clearTimeout(animationStore.interval);
								delete animationStore.interval;
							}
						}
					});
					listener.observe(wishAPIDivLeft, {attributes: true});

					const listener2 = new MutationObserver((..._args) => {
						if (menuDiv.classList.contains("hidden") && animationStore.interval) {
							clearTimeout(animationStore.interval);
							delete animationStore.interval;
						} else {
							if (wishAPIDivLeft.classList.contains("active")) {
								animationStore.interval = setTimeout(animationStore.func, 1, animationStore);
							}
							else if (animationStore.interval) {
								clearTimeout(animationStore.interval);
								delete animationStore.interval;
							}
						}
					});
					listener2.observe(menuDiv, {attributes: true});
				}
				else {
					const listener = new MutationObserver((..._args) => {
						if (wishAPIDivLeft.classList.contains("active")) {
							wishAPIDivLeft.style.backgroundImage = `url("${lib.assetURL}extension/wishAPI/res/image/util/title_bar_off_active.png")`;
							wishAPIDivLeft.style.color = "#000000";
						} else {
							wishAPIDivLeft.style.backgroundImage = `url("${lib.assetURL}extension/wishAPI/res/image/util/title_bar_off.png")`;
							wishAPIDivLeft.style.color = "#FFFFFF";
						}
					});
					listener.observe(wishAPIDivLeft, {attributes: true});
				}

				for (let j = 1; j < wishAPIDivRight.childElementCount; ++j) {
					const div = wishAPIDivRight.childNodes[j];
					div.style.color = "#FFFFFF";
					div.style.fontFamily = `"ansory-contentCN"`
				}

				wishAPIDivRight.childNodes[wishAPIDivRight.childElementCount - 3].style.fontFamily = "ansory-copyrights";

				let enableDiv = wishAPIDivRight.childNodes[1];
				enableDiv.childNodes[1].classList.add("ansory");

				let debugDiv = wishAPIDivRight.childNodes[2];
				debugDiv.childNodes[1].classList.add("ansory");

				wishAPIDivRight.childNodes[3].style.fontFamily = "ansory-info"

				let licenseDiv = wishAPIDivRight.childNodes[5];
				licenseDiv.removeChild(licenseDiv.childNodes[0]);
				licenseDiv.style.fontFamily = null;

				const licenseSpan = ndzdi.span([
					ndzdi.div(".", {fontFamily: "ansory-contentCN"}, "开源协议"),
					ndzdi.div(".", "&nbsp;", true),
					ndzdi.div(".", {fontFamily: "ansory-symbol", fontSize: "17px"}, "=>")
				], licenseDiv);

				licenseDiv.link = ndzdi.div(".config", {
					...style,
					textAlign: "center",
					border: "2px solid white",
					fontFamily: "ansory-license"
				});
				ndzdi.span([
					ndzdi.div(lib.extensionMenu["extension_wishAPI"].license.info, {fontSize: "24px"}, true)
				], licenseDiv.link);

				licenseDiv.addEventListener(lib.config.touchscreen ? "touchend" : "click", () => {
					if (licenseDiv.classList.contains("on")) {
						wishAPIDivRight.insertBefore(licenseDiv.link, licenseDiv.nextSibling);
						licenseSpan.childNodes[2].style.transform = "rotate(90deg)";
					} else {
						if (wishAPIDivRight.childNodes.contains(licenseDiv.link)) wishAPIDivRight.removeChild(licenseDiv.link);
						licenseSpan.childNodes[2].style.transform = null;
					}
				});

				const outfromDiv = wishAPIDivRight.childNodes[6];
				outfromDiv.removeChild(outfromDiv.childNodes[0]);
				outfromDiv.style.fontFamily = null;

				const outfromSpan = ndzdi.span([
					ndzdi.div(".", {fontFamily: "ansory-contentCN"}, "开源许可证"),
					ndzdi.div(".", "&nbsp;", true),
					ndzdi.div(".", {fontFamily: "ansory-symbol", fontSize: "17px"}, "=>")
				], outfromDiv);

				outfromDiv.links = ndzc.a;
				for (const name in lib.extensionMenu["extension_wishAPI"].outfrom.info) {
					const {
						web,
						type,
						content
					} = lib.extensionMenu["extension_wishAPI"].outfrom.info[name];
					let div = ndzdi.div([
						ndzdi.span([
							ndzdi.div(name, {
								fontFamily: ndzp.e("c", type) ? "ansory-contentCN" : "ansory-contentEN",
								fontSize: ndzp.e("c", type) ? "18px" : "30px"
							}),
							ndzdi.div("&nbsp;", true),
							ndzdi.div("=>", {
								fontFamily: "ansory-symbol",
								fontSize: "17px"
							}),
							ndzdi.br,
							ndzdi.div(web, {
								fontFamily: "ansory-website",
								fontSize: "11px"
							})
						])
					], style, ".config.pointerspan");
					div.link = ndzdi.div([
						ndzdi.span([
							ndzdi.div(content, {fontSize: "24px", textAlign: "center"}, true)
						])
					], {
						...style,
						border: "1.5px solid white",
						fontFamily: "ansory-license"
					}, ".config");

					div.addEventListener(lib.config.touchscreen ? "touchend" : "click", () => {
						div.classList.toggle("on");
						if (div.classList.contains("on")) {
							wishAPIDivRight.insertBefore(div.link, div.nextSibling);
							div.childNodes[0].childNodes[2].style.transform = "rotate(90deg)";
						} else {
							wishAPIDivRight.removeChild(div.link);
							div.childNodes[0].childNodes[2].style.transform = null;
						}
					})

					outfromDiv.links.push(div);
				}

				outfromDiv.addEventListener(lib.config.touchscreen ? "touchend" : "click", () => {
					if (outfromDiv.classList.contains("on")) {
						outfromDiv.links.reverse().forEach(div => {
							if (div.classList.contains("on")) wishAPIDivRight.insertBefore(div.link, outfromDiv.nextSibling);
							wishAPIDivRight.insertBefore(div, outfromDiv.nextSibling);
						});
						outfromDiv.links.reverse()
						outfromSpan.childNodes[2].style.transform = "rotate(90deg)";
					} else {
						outfromDiv.links.forEach(div => {
							if (wishAPIDivRight.childNodes.contains(div)) wishAPIDivRight.removeChild(div);
							if (div.classList.contains("on") && wishAPIDivRight.childNodes.contains(div.link)) wishAPIDivRight.removeChild(div.link);
						});
						outfromSpan.childNodes[2].style.transform = null;
					}
				});
			}
			else {
				alert("wishAPI加载失败\n准备重启\n（不想重启就别装Dev版）\n（什么只有Dev版？没事了（");
				setTimeout(game.reload, 10);
			}
		},
		async precontent(extension) {
			await lib.ansorial.ndz.waitReady;

			if (extension.enable && lib.extensionMenu["extension_wishAPI"]) {

				delete lib.extensionMenu["extension_wishAPI"].enable;
				delete lib.extensionMenu["extension_wishAPI"].delete;

				await Promise.all(ndzf.addFontsInObject({
					"ansory-name": `url("${lib.assetURL}extension/wishAPI/res/fonts/name.woff2") format("woff2")`,
					"ansory-title": `url("${lib.assetURL}extension/wishAPI/res/fonts/title.woff2") format("woff2")`,
					"ansory-contentCN": `url("${lib.assetURL}extension/wishAPI/res/fonts/contentCN.woff2") format("woff2")`,
					"ansory-contentEN": `url("${lib.assetURL}extension/wishAPI/res/fonts/license.woff2") format("woff2")`,
					"ansory-license": `url("${lib.assetURL}extension/wishAPI/res/fonts/license.woff2") format("woff2")`,
					"ansory-info": `url("${lib.assetURL}extension/wishAPI/res/fonts/info.woff2") format("woff2")`,
					"ansory-website": `url("${lib.assetURL}extension/wishAPI/res/fonts/website.woff2") format("woff2")`,
					"ansory-symbol": `url("${lib.assetURL}extension/wishAPI/res/fonts/symbol.woff2") format("woff2")`,
					"ansory-copyrights": `url("${lib.assetURL}extension/wishAPI/res/fonts/copyrights.woff2") format("woff2")`
				}));

				ndzs.addObject({
					".config.toggle.on>.ansory": {
						backgroundColor: "rgba(47,101,150,1)"
					},
					".config.toggle>.ansory>div": {
						backgroundImage: "linear-gradient(rgba(75,75,75,1), rgba(70,70,70,1))",
						boxShadow: "rgb(51 51 51) 0 0 0 1px, rgb(0 0 0 / 20%) 0 3px 10px",
					},
					".config.toggle.on>.ansory>div": {
						backgroundImage: "linear-gradient(rgba(75,75,75,1), rgba(70,70,70,1))",
						boxShadow: "rgb(51 51 51) 0 0 0 1px, rgb(0 0 0 / 20%) 0 3px 10px"
					},
					".config.toggle.on>.ansory::before": {
						content: "none"
					}
				});

				// Test
				const textParse = new TextDecoder("UTF-8");

				const map = {
					"extension/wishAPI/LICENSE": str => {
						lib.extensionMenu["extension_wishAPI"].license.info = str
					},
					"extension/wishAPI/res/license/noname": str => {
						lib.extensionMenu["extension_wishAPI"].outfrom.info["无名杀"].content = str
					},
					"extension/wishAPI/res/license/libnodiz": str => {
						lib.extensionMenu["extension_wishAPI"].outfrom.info["Ansory Dizaster Lib"].content = str
					}
				};

				for (const name in map) {
					const file = await ndzfs.readFile(name);
					const str = Buffer.isBuffer(file) ? file.toString() : textParse.decode(file);
					map[name](str
						.replace(/</g, "&lt;")
						.replace(/>/g, "&gt;")
						.replace(/(\n|\r\n)/g, ndzdg.br));
				}

			}
		}
	}
});
